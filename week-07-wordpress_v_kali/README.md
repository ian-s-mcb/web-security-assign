# Project 7 - WordPress Pentesting

Time spent: **X** hours spent in total

> Objective: Find, analyze, recreate, and document **five vulnerabilities** affecting an old version of WordPress

## Pentesting Report

1. Unauthenticated Stored Cross-Site Scripting (XSS)
  - [ ] Summary: 
    - Vulnerability types: XSS
    - Tested in version: 4.2
    - Fixed in version:  4.2.1
  - [ ] GIF Walkthrough: 
    - ![exploit-1-screenshot](/week-07-wordpress_v_kali/exploit-1-screenshot.png?raw=true)
  - [ ] Steps to recreate: 
    - Post a comment on a WP post
    - Use the contents of [this file](/week-07-wordpress_v_kali/exploit-1-comment.txt) as the body of the comment
  - [ ] Affected source code:
    - [Link 1](https://core.trac.wordpress.org/browser/tags/4.2/src/wp-comments-post.php)
1. CVE-2015-5622 - Authenticated Stored Cross-Site Scripting (XSS)
  - [ ] Summary: 
    - Vulnerability types: XSS
    - Tested in version: 4.2
    - Fixed in version: 4.2.3
  - [ ] GIF Walkthrough: 
    - ![exploit-2-screenshot-1](/week-07-wordpress_v_kali/exploit-2-screenshot-1.png?raw=true)
    - ![exploit-2-screenshot-2](/week-07-wordpress_v_kali/exploit-2-screenshot-2.png?raw=true)
  - [ ] Steps to recreate: 
    - Make a post using a WP contributor or author user
    - Use the contents of [this file](/week-07-wordpress_v_kali/exploit-2-post.txt) as the body of the post
  - [ ] Affected source code:
    - [Link 1](https://core.trac.wordpress.org/browser/tags/4.2/src/wp-comments-post.php)
1. CVE-2015-5714 - Authenticated Shortcode Tags Cross-Site Scripting (XSS)
  - [ ] Summary: 
    - Vulnerability types: XSS
    - Tested in version: 4.2
    - Fixed in version: 4.2.5
  - [ ] GIF Walkthrough: 
    - ![exploit-3-screenshot-1](/week-07-wordpress_v_kali/exploit-3-screenshot-1.png?raw=true)
    - ![exploit-3-screenshot-2](/week-07-wordpress_v_kali/exploit-3-screenshot-2.png?raw=true)
  - [ ] Steps to recreate: 
    - Make a post using a WP contributor or author user
    - Use the contents of [this file](/week-07-wordpress_v_kali/exploit-3-post.txt) as the body of the post
  - [ ] Affected source code:
1. (Optional) Vulnerability Name or ID
  - [ ] Summary: 
    - Vulnerability types:
    - Tested in version:
    - Fixed in version: 
  - [ ] GIF Walkthrough: 
  - [ ] Steps to recreate: 
  - [ ] Affected source code:
    - [Link 1](https://core.trac.wordpress.org/browser/tags/version/src/source_file.php)
1. (Optional) Vulnerability Name or ID
  - [ ] Summary: 
    - Vulnerability types:
    - Tested in version:
    - Fixed in version: 
  - [ ] GIF Walkthrough: 
  - [ ] Steps to recreate: 
  - [ ] Affected source code:
    - [Link 1](https://core.trac.wordpress.org/browser/tags/version/src/source_file.php) 

## Assets

List any additional assets, such as scripts or files

## Resources

- [WordPress Source Browser](https://core.trac.wordpress.org/browser/)
- [WordPress Developer Reference](https://developer.wordpress.org/reference/)

GIFs created with [LiceCap](http://www.cockos.com/licecap/).

## Notes

Describe any challenges encountered while doing the work

## License

    Copyright [yyyy] [name of copyright owner]

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
