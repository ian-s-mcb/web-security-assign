# Project 9 - Honeypot

Time spent: 6 hours spent in total

> Objective: Setup a honeypot and intercept some attempted attacks in
> the wild.

## Setting up the MHN admin and Honeypot instances

Although the cloud instance provisioning went smoothly, since I chose a
cloud provider I've used before (AWS), I did run into one issue with the
MHN admin application installer script. The script referenced a GitHub
repo that was no longer accessible. Fortunately, the issue had already
been reported to MHN on their GitHub issue tracker and a workaround had
been conceived. Once I applied the workaround, which was a one-line
change to a script in the install folder of the MHN repo, the
installation finished successfully.

The following screenshot shows:

1. AWS EC2 instances in the web management console 
2. AWS EC2 security groups that describe firewall rules for the admin
instance
3. AWS EC2 security groups that describe firewall rules for the honeypot
instance

![section-1-first_honeypot-screenshot-1](/week-09-honeypot/screenshots/section-1-first_honeypot-screenshot-1.gif?raw=true)

Installing the next component, the first honeypot (Dionaea over HTTP),
was a quick step, but it gave deceiving results that took additional
work to correct. Within the first few minutes of the honeypot being
online, one activity was detected on it and shown in MHN's attack
reports page. This was a good sign, but unfortunately, I could not
generate any attack activity by targeting the honeypot with the nmap
command. I decided to let the nmap issue be and wait for more random
attack activity, but none occured, even after waiting two days. That's
when I decided to reprovision the honeypot (which had no effect) and
reprovision the admin app (which indeed worked). With the instance
reprovisioned and the honeypot online, I received numerious random
activity within minutes. Also, running the nmap command triggered reams
of activity.

The following screenshot shows that activity in the MHN attack report
page.

![section-1-first_honeypot-screenshot-2](/week-09-honeypot/screenshots/section-1-first_honeypot-screenshot-2.gif?raw=true)

## Additional honeypot types

Two additional honeypot types were applied: Suricata and Snort. No
issues were encountered with these honeypots. Unlike the Dionaea type,
these two types produced payload reports. These reports didn't provide
much information, but unlike the attack reports, these had a signature
field for each entry. Unfortuntately, most of the payload signatures
were hard to understand, like "ET DROP Dshield Block Listed Source group
1" for the Suricata honeypot or "ET CINS Active Threat Intelligence Poor
Reputation IP TCP group 7" for the Snort honeypot.

The following screenshot shows the attack and payload reports from the
Suricata honeypot:

![section-2-bonus_honeypots-1-screenshot](/week-09-honeypot/screenshots/section-2-bonus_honeypots-1-screenshot.gif?raw=true)

The following screenshot shows the attack and payload reports from the
Snort honeypot:

![section-2-bonus_honeypots-2-screenshot](/week-09-honeypot/screenshots/section-2-bonus_honeypots-2-screenshot.gif?raw=true)

## Summary of data collected

* Number of attacks:
  * Dionaea
    * 297 attacks
    * 60 min of runtime
  * Suricata
    * 21 attacks
    * 30 min of runtime
  * Snort
    * 32 attacks
    * 30 min of runtime

* The following shows the MHN sensor summary page:

  ![section-3-summary-screenshot](/week-09-honeypot/screenshots/section-3-summary-screenshot.png?raw=true)

* [Attacked info exported as a JSON file](/week-09-honeypot/attacks.json)
